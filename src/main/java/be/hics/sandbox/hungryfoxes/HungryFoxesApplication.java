package be.hics.sandbox.hungryfoxes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HungryFoxesApplication {

	public static void main(String[] args) {
		SpringApplication.run(HungryFoxesApplication.class, args);
        hungryFoxesPrettyPrint("CCC[CCC]FCC[CCCCC]CFFFF[CCC]FFFF");
        hungryFoxesPrettyPrint("...[CCC]...[CCCFC].....[CCC]....");
        hungryFoxesPrettyPrint("CCC[CCC]FCC[CCCFC]CFFFF[CCC]FFFF");
        hungryFoxesPrettyPrint("CCC[]FCC[]CFFFF[CCC]FFFF[CCCCCC]");
        hungryFoxesPrettyPrint("CCC[[[]]]FCC[]F[CCC]FFFF[C[C]CC]");
        hungryFoxesPrettyPrint("[C][[[]]]FCC[]F[CCC]FFFF[C[C]CC]");
        hungryFoxesPrettyPrint("F[C..[C.]..C]....[C]..[C..]");
        hungryFoxesPrettyPrint("[C][.C.C.CC..CC...C.CC....");
        hungryFoxesPrettyPrint("[CC.......CC...CC.CCC[C...C]C...CCCCC....C......C].");
    }

    private static void hungryFoxesPrettyPrint(final String farm) {
        System.out.println(String.format("%s next day => %s", farm, Dinglemouse.hungryFoxes(farm)));
    }
}
