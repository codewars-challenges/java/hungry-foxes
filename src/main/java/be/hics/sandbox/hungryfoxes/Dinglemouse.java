package be.hics.sandbox.hungryfoxes;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * https://www.codewars.com/kata/the-hunger-games-foxes-and-chickens/train/java
 * Kata
 * <p>
 * Story
 * Old MacDingle had a farm.
 * To be more precise, he had a free-range chicken farm.
 * But Old MacDingle also had a fox problem.
 * Foxes F eat chickens C
 * At night the only guaranteed "safe" chickens are in their cages [] (unless a fox has got into the cage with them!)
 * <p>
 * Kata Task
 * Given the initial configuration of foxes and chickens what will the farm look like the next morning after the hungry foxes have been feasting?
 * <p>
 * Examples
 * Ex1	Before CCC[CCC]FCC[CCCCC]CFFFF[CCC]FFFF
 * After  ...[CCC]F..[CCCCC].FFFF[CCC]FFFF
 * Ex2	Before ...[CCC]...[CCCFC].....[CCC]....
 * After  ...[CCC]...[...F.].....[CCC]....
 * Ex3	Before CCC[CCC]FCC[CCCFC]CFFFF[CCC]FFFF
 * After  ...[CCC]F..[...F.].FFFF[CCC]FFFF
 * <p>
 * Notes
 * Anything not a fox, a chicken, or a cage is just dirt .
 * All cages are intact (not open-ended), and there are no cages inside other cages
 */
public class Dinglemouse {

    private static final BiFunction<Integer, Character, Tuple<Integer, String>> createTuple = (i, c) -> new Tuple(i, String.valueOf(c));

    public static String hungryFoxes(final String farm) {
        List<Tuple<Integer, String>> cageStructure = generateCageStructure(farm);
        List<Integer> walkers = generateWalkers(cageStructure);
        return cageStructure.stream().map(t -> t.getV().compareTo("C") == 0 ? walkers.contains(t.getU()) ? "." : t.getV() : t.getV()).collect(Collectors.joining());
    }

    private static List<Tuple<Integer, String>> generateCageStructure(final String farm) {
        List<Tuple<Integer, String>> cageStructure = new ArrayList<>();
        int cumulativeCageNumber = 0;
        int cumulativeCageLevel = 0;
        int cageNumber = 0;
        for (char c : farm.toCharArray()) {
            switch (c) {
                case '[':
                    cumulativeCageNumber += 1;
                    cumulativeCageLevel += 1;
                    cageNumber = cumulativeCageNumber;
                    cageStructure.add(createTuple.apply(cageNumber, c));
                    break;
                case ']':
                    cageStructure.add(createTuple.apply(cageNumber, c));
                    cumulativeCageLevel -= 1;
                    cageNumber = cumulativeCageLevel;
                    break;
                default:
                    cageStructure.add(createTuple.apply(cageNumber, c));
                    break;
            }
        }
        return cageStructure;
    }

    private static List<Integer> generateWalkers(final List<Tuple<Integer, String>> cageStructure) {
        return cageStructure.stream().filter(t -> t.getV().compareTo("F") == 0).map(Tuple::getU).distinct().collect(toList());
    }

    private static class Tuple<U, V> {
        private final U u;
        private final V v;

        public Tuple(final U u, final V v) {
            this.u = u;
            this.v = v;
        }

        public U getU() {
            return u;
        }

        public V getV() {
            return v;
        }
    }

}
